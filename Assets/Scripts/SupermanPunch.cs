using UnityEngine;

public class SupermanPunch : MonoBehaviour
{
    /// <summary>
    /// ���� �����
    /// </summary>
    public float punchForce;

    private int badGuyLayer;
    private EnemyCounter counter;

    private void Start()
    {
        badGuyLayer = LayerMask.NameToLayer("BadGuy");
        counter = GameObject.Find("BadGuysCounter").GetComponent<EnemyCounter>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == badGuyLayer)
        {
            if(collision.gameObject.TryGetComponent<Rigidbody>(out var collisionRigidBody))
            {
                var punchVector = (collision.gameObject.transform.position - this.gameObject.transform.position).normalized;
                collisionRigidBody.AddForce(punchVector * punchForce, ForceMode.Impulse);

                if (collision.gameObject.TryGetComponent<EnemyController>(out var enemyController))
                {
                    enemyController.Kill();
                    counter.Decrease();
                }
            }
        }
    }
}
