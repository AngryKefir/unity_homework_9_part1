﻿using UnityEngine;
using UnityEngine.UI;

public class EnemyCounter : MonoBehaviour
{
    private Text counterText;
    private int value;

    private void Start()
    {
        counterText = GetComponent<Text>();
    }

    public void Decrease()
    {
        value--;
        UpdateText();
    }

    public void Init(int enemiesCount)
    {
        value = enemiesCount;
        UpdateText();
    }

    private void UpdateText()
    {
        counterText.text = value.ToString();
    }
}
