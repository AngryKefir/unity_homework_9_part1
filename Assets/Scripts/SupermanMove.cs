using UnityEngine;

public class SupermanMove : MonoBehaviour
{
    public float sensitivityHor = 9.0f;
    public float sensitivityVert = 9.0f;
    public float minimumVert = -80.0f;
    public float maximumVert = 80.0f;

    public float speed = 20;
    public float speedBoostCoef = 2f;

    private float _rotationX = 0;
    private Rigidbody _rigidbody;

    private void Start()
    {
        _rigidbody = this.GetComponent<Rigidbody>();
        _rigidbody.maxAngularVelocity = 0f;
    }

    private void FixedUpdate()
    {
        float movingSpeed = Input.GetMouseButton(0) ? speed * speedBoostCoef : speed;
        _rigidbody.velocity = this.transform.forward.normalized * movingSpeed;
    }

    private void Update()
    {
        _rotationX -= Input.GetAxis("Mouse Y") * sensitivityVert;
        _rotationX = Mathf.Clamp(_rotationX, minimumVert, maximumVert);
        float delta = Input.GetAxis("Mouse X") * sensitivityHor; 
        float rotationY = transform.localEulerAngles.y + delta; 
        transform.localEulerAngles = new Vector3(_rotationX, rotationY, 0);
    }
}