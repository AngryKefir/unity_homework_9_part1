﻿using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float deathTime = 1;
    private bool isPunched = false;

    private void Update()
    {
        if(isPunched)
        {
            isPunched = false;
            Destroy(this.gameObject, deathTime);
        }
    }

    public void Kill()
    {
        isPunched = true;
        if(TryGetComponent<AudioSource>(out var audio))
        {
            audio.Play();
        }
        this.gameObject.layer = 0;
    }
}
