using System;
using UnityEngine;

public class HeroesSpawn : MonoBehaviour
{
    public GameObject[] prefabs;
    public Rect rect;
    public int objectsCount;
    public float height;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;

        var rnd = new System.Random((int)DateTime.Now.Ticks);
        int enemiesCount = 0;
        int badGuyLayer = LayerMask.NameToLayer("BadGuy");

        for (int i = 0; i < objectsCount; i++)
        {
            var randomPrefab = prefabs[rnd.Next(prefabs.Length)];
            if(randomPrefab.layer == badGuyLayer)
            {
                enemiesCount++;
            }

            var randomPosition = new Vector3(
                x: rnd.Next((int)rect.xMin, (int)rect.xMax), 
                y: height, 
                z: rnd.Next((int)rect.yMin, (int)rect.yMax));

            var randomRotation = Quaternion.Euler(new Vector3(0, rnd.Next(360), 0));

            Instantiate(randomPrefab, randomPosition, randomRotation, this.transform);
        }

        var counter = GameObject.Find("BadGuysCounter").GetComponent<EnemyCounter>();
        counter.Init(enemiesCount);
    }
}
